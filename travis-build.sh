### Basic packages

apt -yy update
apt -yy dist-upgrade
apt -yy install wget

### Environment variables
export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true
shopt -s extglob

### Install Dependencies
apt -yy install devscripts lintian gnupg2 build-essential automake autotools-dev equivs kwin libkdecorations2-dev libkf5guiaddons-dev libkf5configwidgets-dev libqt5x11extras5-dev pkg-config --no-install-recommends
mk-build-deps -i -t "apt-get --yes" -r

### Build Deb
mkdir travis-build-source
mv !(travis-build-source) travis-build-source/ # Hack for debuild
cd travis-build-source
debuild -b -uc -us

ls -lt
ls -lt ..
